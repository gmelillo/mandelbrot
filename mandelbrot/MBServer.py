from pycaldis import Server, Color, JobStatus, TaskStatus, E_NoLogin, E_CredNonVald
from PIL      import Image
import math
import threading

from colorsys import hsv_to_rgb

class MBServer( Server ):

  def __init__( self, port=4096, task_per_job=1000, c=complex( 0.0, 0.0 ), r=2.0, maxiter=100000, dots=16 ):
    super( MBServer, self ).__init__( port, task_per_job )

    # Start

    self.printalert	= True
    self.printdebug	= False
    self.printerr	= True

    # Se non ci sono job memorizzati li creo secondo i dati in ingresso altrimenti uso quelli memorizzati
    if len( self.data['jobs_io'] ) == 0:
      self.alert( "Creo i JOB" )
      self.centro     = c
      self.raggio     = r
      self.iterazioni = maxiter
      self.punti      = dots
  
      self.rmin       = self.centro.real - self.raggio 
      self.rmax       = self.centro.real + self.raggio 
      self.imin       = self.centro.imag - self.raggio 
      self.imax       = self.centro.imag + self.raggio 

      self.CreateJob( c, r, maxiter, dots )

    else:

      if len( self.jobs ) == 0:
        # Forse i jobs erano stati tutti completati e quindi non c'e' nessun datafile caricati in seld.jobs
        self.jobread( 0, self.data['jobs_io'][ 0 ] )
        
      for jid, j in self.jobs.items():
        # Uso i dati del primo job. E' un baco che si risolve raggruppando i job in un project
        break

      self.alert( "Ho trovato i JOB" )
         
      # dati validi
      j = self.jobs[ jid ]
      self.centro     = j['data'][0]
      self.raggio     = j['data'][1]
      self.iterazioni = j['data'][2]
      self.punti      = j['data'][3]
  
      self.rmin       = self.centro.real - self.raggio 
      self.rmax       = self.centro.real + self.raggio 
      self.imin       = self.centro.imag - self.raggio 
      self.imax       = self.centro.imag + self.raggio 

      # Unload possibilli job DONE
      for jid, j in self.jobs.items():
        if j['status'] in [ JobStatus.DONE ]:
          self.jobunload( jid )
          
      # Reset possibili job caricati in stato WORKING
      for jid, j in self.jobs.items():
        if j['status'] in [ JobStatus.WORKING ]:
          self.alert( "Resetto job {0}".format( jid ) )
          for i, t in j['tasks'].items():
            t['status']   = TaskStatus.NEW
            t['result']   = []
            t['start']    = 0
            t['end']      = 0
          j['status']     = JobStatus.NEW
          j['sent']       = 0
          j['received']   = 0
          j['task_done']  = 0
          self.data['jobs_io'][ jid ]['changed'] = True
   
    # Creti i job faccio partire i servizi
    self.start_internalcheck()
    self.start_autosave( 600 )
 
    # Statistiche tramite callback di JobDone
    if 'stats' not in self.data:
      self.data['stats']    = {'items': 0, 'iteraz_min': -1, 'iteraz_max': -1, 'histogram': {}}
    self.cb_jobdone_stats = self.JobDoneStats

  def __del__( self ):
    super( MBServer, self ).__del__()

  def CreateJob( self, c=complex( 0.0, 0.0 ), r=2.0, i=1000, p=1 ):

    def jobadd( self, j ):
      self.jobadd( j )
      print " creato job {0}".format( j['id'] )
      
    th = []
    n_task_ins = 0
    r2 = ( 2 * self.raggio )
    for ii in range( 0, self.punti ):
      ci = self.imin + ( ( r2 * ii ) / self.punti )
      for ir in range( 0, self.punti ):
        cr = self.rmin + ( ( r2 * ir ) / self.punti )
        # Creo job
        if n_task_ins == 0:
          j = self.jobcreate( data=[c, r, i, p], desc='stage0', ffile="taskcompute.py" )
        # Creo e aggiungo task
        t=self.taskcreate( [cr, ci, self.iterazioni, ir, ii] )
        self.taskadd( j, t )
        n_task_ins += 1

        # Aggiungo job in thread separato per gestire parallelamente eventuale unload
        if n_task_ins == self.max_tasks_per_job or ( ii == self.punti -1 and ir == self.punti -1 ):
          t = threading.Thread( name='CreateJob', target=jobadd, args=( self, j, ) )
          th.append( t )
          t.start()
          n_task_ins = 0

    # Attendo fine threads
    for t in th:
      t.join()
    
  '''
    Statistiche
  '''

  def JobDoneStats( self, j, t ):
    '''
      callback richiamata da pycaldis.Server.JobDone
    '''

    # TODO spostare le statistiche in struttura dati project
    #   Raggruppa i dati di tutti i job
 
    self.data['stats']['items'] += 1
    res = t['result'][0]
    if self.data['stats']['iteraz_min'] > res or self.data['stats']['iteraz_min'] == -1:
      self.data['stats']['iteraz_min'] = res
    if self.data['stats']['iteraz_max'] < res or self.data['stats']['iteraz_max'] == -1:
      self.data['stats']['iteraz_max'] = res
    if res not in self.data['stats']['histogram']:
      self.data['stats']['histogram'][ res ] = 0
    self.data['stats']['histogram'][ res ] += 1

  def ShowStats( self ):
    for j in self.data['stats']['jobs']:
      
      print Color.GREEN + "\nSTATS:" + Color.RESET
      print "\tItems: {0} Min Iter: {1} Max Iter: {2}".format( j['items'], j['iteraz_min'], j['iteraz_max'] )
      s = sorted( j['histogram'] )
      print s
      for i in s:
        n = j['histogram'][ i ]       
        print "\t{0:>6d} : {1}".format( i, '*'*n )

  '''
    Export bmp
  '''

  #def mapXY( self, x, y ):
  #  # Dato il punto x,y calcola il numero complesso corrispondente all'interno del piano self.rmin, self.imin, self.rmax, self.imax
  #  a = ( self.raggio * 2 ) / ( self.punti - 1 )
  #  r = self.rmin + a * x
  #  i = self.imin + a * ( self.punti - 1 - y )
  #  return complex( r, i )

  #def mapZ( self, c ):
  #  # Mappa il numero complesso c con le coordinate ( x, y ) del canvas self.punti * self.punti
  #  # restituisce la tupla ( x, y )
  #  x = round( ( self.punti - 1 ) * (       ( c.real - self.rmin ) / ( self.rmax - self.rmin )   ) )
  #  y = round( ( self.punti - 1 ) * ( 1 - ( ( c.imag - self.imin ) / ( self.imax - self.imin ) ) ) )
  #  return ( int( x ), int( y ) )

  def Filldata( self ):

    def SetCanvas( p ):
      # p e' il numero di punti di lato della griglia ( p x p )
      self.punti      = p
      self.totpunti   = p*p
      self.filled     = 0
      self.mb         = [[-1 for y in xrange( self.punti )] for x in xrange( self.punti )]

    def fillXY( x, y, i ):
      try:
        self.mb[y][x]   = i
        self.filled    += 1
      except Exception as e:
        print e
        print "y: {0} y: {1} i: {2} leny: {3} lenx: {4}".format( y, x, i, len( self.mb ), len( self.mb[0] ) )

    # Preparo i dati per creare l' immagine
    self.alert( "Set canvas" )
    SetCanvas( self.punti )

    # Copio i dati validi
    self.alert( "data -> img buf" )
    dv = 0
    for jdid, jdata in self.data['jobs_io'].items():
      if jdata['status'] == JobStatus.DONE:
        self.alert( "Load datafile {0}".format( jdid ) )
        self.jobread( jdid, jdata )
        for jid, j in self.jobs.items():
          if j['status'] == JobStatus.DONE:
            for tid, t in j['tasks'].items():
              if t['status'] == TaskStatus.DONE:
                x = t['data'][3]
                y = t['data'][4]
                fillXY( x, y, t['result'][0] )
                dv += 1
                print "\033[F   {0:>5.2f} %   ".format( 100.0 * dv / ( self.punti*self.punti ) )
        self.alert( "Unload datafile {0}".format( jdid ) )
        self.jobunload( jdid )

  def Export( self, f=None, log=True ):

    def __hue2rgb( hue, saturation=1, value=0.8, rgb_scale = 1 ):
      """
      Given a hue, this function returns a RGB value.
      hue is a number between 0 and 1 that represents a color on the color wheel in RGB order.
      hue2rgb( 0 ) = red,  hue2rgb( 1/3 ) = green,  hue2rgb( 2/3 ) = blue
      """
      return tuple( rgb_scale*c for c in hsv_to_rgb( hue, saturation, value ) )  


    if self.punti > 0:
      if self.data['stats']['iteraz_max'] > self.data['stats']['iteraz_min']:
        
        if f is None:
          f = "{0:.8f}_{1:.8f}i_{2:.8f}_{3}_{4}.bmp".format( self.centro.real, self.centro.imag, self.raggio, self.punti, self.iterazioni )
        if log:
          print "   Export {0}".format( f )
        
        im = Image.new( "RGB", ( self.punti, self.punti ), ( 0,0,0 ) )
        
        try:
          vm = math.log( self.data['stats']['iteraz_min'] )
        except:
          print "errore math.log( {0} )".format( self.data['stats']['iteraz_max'] )
          vm = 0
        a = 1.0 / ( math.log( self.data['stats']['iteraz_max'] ) - vm )
        ap= 100.0 / ( self.punti*self.punti ) # Costante relativa al numero di punti per visualizzare la % di completamento
        if log:
          print ""
        for y in range( self.punti ):
          for x in range( self.punti ):
            v = self.mb[y][x]
            if v > 0 and v < self.iterazioni:
              m = a * ( math.log( v ) - vm )
              rgb = __hue2rgb( m, 1.0, 0.8, 1.0 )
              r = int( 255 * rgb[0] )
              g = int( 255 * rgb[1] )
              b = int( 255 * rgb[2] )
              im.putpixel( ( x, y ), ( r,g,b ) )
            elif v < 0:
              im.putpixel( ( x, y ), ( 255,255,255 ) )
            if log:
              print "\033[F   {0:>5.2f} %   ".format( ap*( y*self.punti+x ) )
        
        try:
          im.save( f, "BMP" )
        except Exception as e:
          print "Error {0}".format( e )
      else:
        if log:
          print "Zona {0} {1}, poche iterazioni".format( self.centro, self.raggio )
          print "   v_min: {0} v_max: {1}".format( self.data['stats']['iteraz_min'], self.data['stats']['iteraz_max'] )
    else:
      print "Errore nei dati"
      print "   Punti: {0}".format( self.punti )

