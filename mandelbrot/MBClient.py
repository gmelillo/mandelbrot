from pycaldis import Client, Color, JobStatus, TaskStatus, E_NoLogin, E_CredNonVald, E_NothingToDo
import time
import socket

class MBClient( Client ):

  def __init__( self, host='127.0.0.1', port=4096 ):
    super( MBClient, self ).__init__( host, port )

    self.printalert	= True
    self.printdebug	= False
    self.printerr	= True

    if 'clientname' not in self.data:
      try:
        self.Signup()
      except:
        self.alert( "Errore signup" )

    if 'clientname' in self.data:
      try:
        self.Login()
      except:
        self.alert( "Errore login" )

    while True:
      try:
        self.Run()
      except socket.error as e:
        if e.errno == 111:
          self.alert( "Connessione rifiutata" )
        time.sleep( 60 )
      except Exception as e:
        self.printErr( Err = e )
        time.sleep( 60 )
      except:
        self.alert( "errore Run" )
        time.sleep( 60 )

  def __del__( self ):
    super( MBClient, self ).__del__()

  def Run( self ):
 
    b_finish = False
    while ( b_finish == False ):
      self.alert( "Chiedo job" )
      try:
        self.JobRequest()

        b_finish = ( len( self.jobs ) == 0 )
        if b_finish == False:
          self.alert( "  Elaboro" )
          self.Compute()
          for idj, j in self.jobs.items():
            if j['status'] == JobStatus.DONE:
              self.alert( "  Invio risultati" )
              self.JobDone()
            else:
              self.alert( "Non invio niente." )
        else:
          self.alert( "Fine" )
          time.sleep( 600 )

      except E_CredNonVald:
        self.alert( "Rieseguo signup" )
        self.Signup()
      except E_NoLogin:
        self.alert( "Rieseguo login" )
        self.Login()
      except E_NothingToDo:
        self.alert( "Non ci sono altri job da eseguire" )
        time.sleep( 600 )
      except:
        raise
