#!/usr/bin/python
from mandelbrot import MBServer, Color, JobStatus, TaskStatus
import math
import os
import time

if 'MB_PORT' in os.environ:
  mb_port   = int( os.environ['MB_PORT'] )
else:
  mb_port   = 4096

if 'MB_TASK' in os.environ:
  mb_task   = int( os.environ['MB_TASK'] )
else:
  mb_task   = 4096

if 'MB_REAL' in os.environ:
  mb_real   = float( os.environ['MB_REAL'] )
else:
  mb_real   = -0.74495470

if 'MB_IMAG' in os.environ:
  mb_imag   = float( os.environ['MB_IMAG'] )
else:
  mb_imag   = 0.09975697

if 'MB_RADI' in os.environ:
  mb_radi   = float( os.environ['MB_RADI'] )
else:
  mb_radi   = 0.0001

if 'MB_MAXI' in os.environ:
  mb_maxi   = int( os.environ['MB_MAXI'] )
else:
  mb_maxi   = 100000

if 'MB_DOTS' in os.environ:
  mb_dots   = int( os.environ['MB_DOTS'] )
else:
  mb_dots   = 64

print "Server: Port:{0} TaskXJob:{1} C:({2}, {3}) R:{4} I:{5} D:{6}".format( mb_port, mb_task, mb_real, mb_imag, mb_radi, mb_maxi, mb_dots )

S=MBServer( port=mb_port, task_per_job=mb_task, c=complex( mb_real, mb_imag ), r=mb_radi, maxiter=mb_maxi, dots=mb_dots )

print Color.PURPLE + "Controllo presenza job da eseguire" + Color.RESET
b_work = False
for jid, j in S.data['jobs_io'].items():
  if j['status'] != JobStatus.DONE:
    b_work = True
    break

if b_work:
  print Color.PURPLE + "Ci sono Job da eseguire" + Color.RESET

  print Color.PURPLE + "Avvio server" + Color.RESET
  S.Listen()

  print Color.PURPLE + "Controllo avanzamento dei jobs" + Color.RESET
  b_complete = False
  while b_complete == False:
    b_complete = True
    for jid, j in S.data['jobs_io'].items():
      if j['status'] in [ JobStatus.NEW, JobStatus.WORKING, JobStatus.RESEND ]:
        b_complete = False
        time.sleep( 5 )
        break
       
  print Color.PURPLE + "Fine elaborazione" + Color.RESET

print Color.PURPLE + "Scrivo datafile" + Color.RESET
S.write()

S.Shutdown()
S.stop_internalcheck()
S.stop_autosave()

print Color.PURPLE + "Statistiche" + Color.RESET
print "Items: {0:>7d} Iterazioni: min: {1:>7d} max: {2:>7d}".format( S.data['stats']['items'], S.data['stats']['iteraz_min'], S.data['stats']['iteraz_max'] )
if S.data['stats']['items'] > 0:
  h={}
  l=10
  for i in range( 1 + int( math.log( S.data['stats']['iteraz_max'], l ) ) ):
    h[ i ] = 0
  for i, n in S.data['stats']['histogram'].items():
    li = int( math.log( i, l ) )
    h[ li ] += n
  sn = 0
  print "Iter            |Punti    |Norm  |" 
  for i, n in h.items():
    sn += n
    pe = round( 1.0 * n / S.data['stats']['items'], 4 )
    print "{0:>6d} - {1:>6d}:|{2:>9d}|{3:<6.4}|{4}".format( int(math.pow(l, i)), int(math.pow(l, i+1))-1, n, pe, "*"*(int(50*pe)) )
  print "                 {0:>9d}".format( sn )
  
  print Color.PURPLE + "Raccolta risultati" + Color.RESET
  S.Filldata()
  
  print Color.PURPLE + "Export" + Color.RESET
  S.Export()

print Color.PURPLE + "Spengo server" + Color.RESET
S.Exit()


