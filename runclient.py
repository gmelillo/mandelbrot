#!/usr/bin/python
from mandelbrot import MBClient
import os

if 'MB_SERVER' in os.environ:
  mb_server = os.environ['MB_SERVER']
else:
  mb_server = '127.0.0.1'
if 'MB_PORT' in os.environ:
  mb_port   = int( os.environ['MB_PORT'] )
else:
  mb_port   = 4096

print "Server: {0}:{1}".format( mb_server, mb_port )

C=MBClient( host=mb_server, port=mb_port )

