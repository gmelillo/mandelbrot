#!/bin/bash
printf "\033[1;32m"
echo "*************************"
echo "* Mandelbrot server     *"
echo "*************************"
printf "\033[0m"

docker image rmi --force busnellistefano/mandelbrotserver:latest
docker image rmi --force busnellistefano/mandelbrotserver:$(uname -m)
docker image prune
docker container prune
docker build -t busnellistefano/mandelbrotserver:$(uname -m) .
docker push busnellistefano/mandelbrotserver:$(uname -m)
#docker tag busnellistefano/mandelbrotserver:$(uname -m) busnellistefano/mandelbrotserver:latest
