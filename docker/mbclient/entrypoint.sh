#!/bin/sh
if [[ -z ${1} ]]; then
  cd /root/
  python runclient.py > client.log
else
  exec $@
fi    
