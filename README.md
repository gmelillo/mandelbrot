# Mandelbrot Set #

Calcolo distribuito dell` insieme di Mandelbrot basato su  pycaldis

## Installazione ##

```
git clone git@bitbucket.org:StefanoBusnelli/pycaldis.git
cd pycaldis
pip install setuptools
pip install -e .
```

## Avvio server ##

Il server rester� in ascolto su tutte le interfacce e sulla porta selezionata.
Se port non viene specificata quella di default � la porta 4096

Dalla shell di python:

```
from mandelbrot import MBServer
S=MBServer( port=4096, task_per_job=2000, c=complex( -0.74495470, 0.09995697 ), r=0.0005, maxiter=10000, dots=256 )
```

Parametri:

* c            = centro dell'insieme di mandelbrot nel piano complesso di Gauss
* r            = raggio dell'insieme da calcolare
* maxiter      = massimo numero di iterazioni per uscire dal loop ricorsivo z=Z^2 + c
* dots         = numero di punti per lato dell' immagine da generare

Parametri derivati da pycaldis.Server:
 
* port         = porta TCP del server
* task_per_job = numero di task con cui comporre ogni job da elaborare

```
root@debian-00:~# netstat -tapn | grep 4096 | grep LISTEN
tcp        0      0 0.0.0.0:4096            0.0.0.0:*               LISTEN      6849/python
```

### Fine elaborazione ###

```
S.Filldata()
S.Export()
```

## Avvio clients ##

I client si connettono al server all' indirizzo ip ed alla porta indicata.
Se host e port non vengono indicati il default � 127.0.0.1:4096

Dalla shell di python ( In folder differenti ):

```
from mandelbrot import MBClient
C=MBClient( host="192.168.1.100", port=4096 )
```

## Esempi: ##

c=complex( -0.74495470, 0.09995697i )
r=0.0005
maxiter=50000
dots=1024

![Alt text](./img/-0.74495470_0.09995697i_0.00050000_1024_50000.bmp?raw=true "-0.7449547+0.09995697j")

c=complex( -0.74495470, 0.09985697i )
r=0.0005 
maxiter=50000
dots=1024

![Alt text](./img/-0.74495470_0.09985697i_0.00050000_1024_50000.bmp/?raw=true "-0.74495470 0.09985697i")

c=complex( -0.744955, 0.099757i )
r=0.0002 
maxiter=20000
dots=1024

![Alt text](./img/-0.744955_0.099757_0.000200_1024_20000.bmp/?raw=true "-0.744955+0.099757j")
