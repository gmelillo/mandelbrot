from MBSet import MBSet
import thread

class MBSetSA( MBSet ):
  def __init__( self, c=None, r=None ):
    super( MBSetSA, self ).__init__( c, r )
  def calcXY( self, x, y ):
    # Inserisce il numero di iterazioni nella posizione corrispondente al punto x,y
    self.lock.acquire( )
    self.threads += 1
    self.lock.release( )
    
    if self.data[y][x] == -1:                       # Se il punto non e' gia' stato calcolato
      self.data[y][x] = -2                          # Flaggo il punto ( x,y ) come in elaborazione
      
      c = self.mapXY( x, y )
      r = self.calcZ( c )
      v = r[0]                                      # Numero di iterazioni
      self.fillXY( x, y, v )
      
    self.lock.acquire( )
    self.threads -= 1
    self.lock.release( )
  def CalcChunk( self, ymin, yptt, xmin, xptt, cname ):
    # Calcola un chunk
    # chunk: ( ymin,yptt,xmin,xptt )
    try:
      if self.iterazioni > 0 and self.punti > 1:
        ap= 100.0 / ( yptt*xptt )
        print "\033[F\033[F{0:>8s}: Calcolo {1} x {2} punti con {3} threads e {4} iterazioni.".format( cname, yptt, xptt, self.maxthreads, self.iterazioni )
        # Calcolo la griglia usando al massimo self.maxthreads per volta
        print ""
        ixy = 0
        for y in range( ymin, ymin+yptt ):
          for x in range( xmin, xmin+xptt ):
            print "\033[F   Threads: {0:>3n} - {1:>6.2f}% It: {2}<->{3}          ".format( self.threads, ap*ixy, self.v_min, self.v_max )
            # Se ci sono self.maxthreads attivi attendo che diminuiscano
            while self.threads >= self.maxthreads:
              pass
            # Lancio un nuovo thread per calvolare il punto y,x
            try:
              thread.start_new_thread( self.calcXY, ( x, y, ) )
            except:
              print "Error: unable to start calcXY thread"
            ixy = ixy + 1
    except Exception:
      print traceback.format_exc( )
  def Calc( self, ExportParziali=0, chunks=8 ):
    # Calcola tutto l'insieme di MB    
    if self.iterazioni > 0 and self.punti > 1:
      print ""
      print ""
      ppc = int ( self.punti / chunks )       # punti per chunk
      #cz  = 0                                 # contatore di chunks fatti
      #acz = chunks * chunks / ExportParziali  # coefficiente per generare numero progressivo da cz
      for cy in range( chunks ):
        ymin = ppc * cy
        if cy + 1 == chunks:
          yppt = self.punti - ( ymin )
        else:
          yppt = ppc
        for cx in range( chunks ):
          xmin = ppc * cx
          if cx + 1 == chunks:
            xppt = self.punti - ( xmin )
          else:
            xppt = ppc
          
          tn = "CH_{0}_{1}".format( cy,cx )
          print "{0} {1} {2} {3} {4}".format( ymin, yppt, xmin, xppt, tn )
          self.CalcChunk( ymin, yppt, xmin, xppt, tn )
          #try:
          #  #tn = "CH_{0}_{1}".format( cy,cx )
          #  print "CalcChunk {0}".format( tn )
          #  thread.start_new_thread( self.CalcChunk, ( ymin, yppt, xmin, xppt, tn ) )
          #except Exception:
          #  print traceback.format_exc( )    
          #except:
          #  print "Error: unable to start self.CalcChunk thread"
          
      while True:
        pass
        
          ## export intermedio
          #if ExportParziali > 1 and cz > 0 and cz % acz == 0:
          #  f = "{0}_{1}_{2}_{3}_{4:0>3n}.bmp".format( self.centro, self.raggio, self.punti, self.iterazioni, int( cz / acz )-1 )
          #  #self.Export( f, False )
          #  try:
          #    thread.start_new_thread( self.Export, ( f, False, ) )
          #  except:
          #    print "Error: unable to start Export thread"
          #
          #cz = cz + 1

      # Attendo la fine di tutti i threads
      cz  = 0                               # contatore di ExportParziali fatti
      while self.filled < self.totpunti and self.threads > 0:
        print "\033[F   Threads: {0:>3n} - {1:>6.2f}% It: {2}<->{3}          ".format( self.threads, 100*self.filled/self.totpunti, self.v_min, self.v_max )
        
        # export intermedio
        if ExportParziali > 1 and self.filled > 0 and ( self.filled/( self.totpunti / ExportParziali ) )>cz == 0:
          f = "{0}_{1}_{2}_{3}_{4:0>3n}.bmp".format( self.centro, self.raggio, self.punti, self.iterazioni, cz )
          cz += 1
          try:
            thread.start_new_thread( self.Export, ( f, False, ) )
          except:
            print "Error: unable to start Export thread"
        
        #time.sleep( 1 )
        pass
