from setuptools import setup

setup(	name		= 'mandelbrot',
	version		= 0.1,
	description	= 'Applicazione client-server per calcolare l\'insieme di Mandelbrot',
	url		= 'https://bitbucket.org/StefanoBusnelli/mandelbrot',
	author		= 'Busnelli Stefano',
	author_email	= 'busnelli.stefano@gmail.com',
	license		= 'GNU GPLv3',
	packeges	= ['mandelbrot'],
        install_requires= ['pycaldis', 'Pillow'],
	zip_safe	= False
)
